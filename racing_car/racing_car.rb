#!/bin/ruby
#
# Complete the 'minimumMovement' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY obstacleLanes as parameter.
#
puts "\n\n\nrunnnig program"

def doBaseDistribution(obstacleList)
    base = []

    obstacleList.each do |item|
      lane1 = if (item == 1) then 1 else 0 end
      lane2 = if (item == 2) then 1 else 0 end
      lane3 = if (item == 3) then 1 else 0 end
      base.push [lane1, lane2, lane3]
    end

    puts "doBaseDistribution > #{base}"
    return base
end

def getNextObstacleFor(baseList, step, lane)
    found = false

    while (baseList.size > step && !found)
        found = (baseList[step][lane] == 1)
        # puts "getNextObstacleFor > value #{baseList[step][lane]} | #{step} | #{found}"
        if found
            break
        end
        step += 1
    end

    puts "getNextObstacleFor > results #{found} | #{step}"
    if found
        return step
    else
        return -1
    end
end

def whichHasBlockerFar(firstLane, secondLane, step, baseList)
    firstLaneObstacleOn = -1
    secondLaneObstacleOn = -1

    firstLaneObstacleOn = getNextObstacleFor(baseList, step, firstLane)
    if firstLaneObstacleOn == -1
        return firstLane
    end

    secondLaneObstacleOn = getNextObstacleFor(baseList, step, secondLane)
    if secondLaneObstacleOn == -1
        return secondLane
    end

    if firstLaneObstacleOn >= secondLaneObstacleOn
        return firstLane
    else
        return secondLane
    end
end

def getNextBestLane(current, stepFrom, base)
    lane1 = 0
    lane2 = 1
    lane3 = 2

    case current
    when lane1
        return whichHasBlockerFar(lane2, lane3, stepFrom, base)
    when lane2
        return whichHasBlockerFar(lane1, lane3, stepFrom, base)
    when lane3
        return whichHasBlockerFar(lane1, lane2, stepFrom, base)
    end
end

def isThereAnyBlocker(lane, position, obstacles)
    foundOnPosition = getNextObstacleFor(obstacles, position, lane)
    found = (foundOnPosition != -1)

    puts "isThereAnyBlocker > block results #{found} position #{foundOnPosition}"
    return found, foundOnPosition
end

def minimumMovement(obstacleLanes)
    numberOfChanges = 0

    puts "obstacles #{obstacleLanes}"
    _baseDistribution = doBaseDistribution obstacleLanes

    step = 0
    previousLane = 1
    currentLane = 1

    loopsCount = 0
    isBlocked = true

    while (isBlocked && step < _baseDistribution.size)
        puts "minimumMovement > loop #{loopsCount} current lane #{currentLane}"

        isBlocked, stepBlockedOn = isThereAnyBlocker(currentLane, step, _baseDistribution)

        if isBlocked
            step = stepBlockedOn + 1

            currentLane = getNextBestLane(currentLane, step, _baseDistribution)
        end

        if currentLane != previousLane
            numberOfChanges += 1
            previousLane = currentLane
        end
        loopsCount += 1
    end

    return numberOfChanges
end

entryValues = [1, 3, 1, 1, 2, 3, 2, 2, 2, 2, 3, 2, 2, 3, 3, 2, 3, 2, 2, 3, 1, 1, 3, 2, 3, 1, 1, 1, 2, 1, 2, 3, 2, 1, 2, 1, 3, 2, 3, 3, 3, 1, 2, 2, 1, 1, 2, 1, 3, 2, 1, 3, 3, 2, 3, 3, 1, 3, 1, 2, 3, 1]
value = minimumMovement entryValues
puts "resultado 1 > #{value}"

# entryValues = [1, 3, 2, 3, 3, 2, 1, 2, 2, 3, 1, 1, 3, 1, 3, 1, 1, 3, 3, 3, 1, 2, 1, 1, 1, 1, 1, 1, 3, 2, 1, 1, 3, 2, 3, 2, 1, 3, 2, 3, 3, 2, 1, 1, 3, 1, 3, 3, 2, 3, 1, 3, 1, 3, 2, 2, 3, 1, 1, 2, 1, 2, 2, 2, 3, 2, 3, 1, 2, 2, 3, 1, 1, 3, 1, 3, 2, 2, 2, 1, 1, 2, 3, 3, 2, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 2, 1, 2, 3, 2, 2, 3, 2, 3, 1, 1, 1, 2, 1, 3, 3, 2, 3, 1, 2, 2, 3, 2, 1, 3, 1, 2, 2, 3, 3, 3, 1, 1, 3, 1, 2, 3, 2, 3, 2, 3, 1, 3, 2, 3, 3, 1, 1, 3, 3, 1, 1, 2, 3, 2, 3, 1, 3, 1, 1, 1, 1, 2, 2, 3, 1, 1, 1, 3, 2, 1, 3, 1, 1, 3, 3, 3, 3, 2, 2, 2, 2, 1, 1, 3, 3, 3, 3, 2, 3, 2, 2, 1, 3, 1, 3, 1, 3, 2, 2, 3, 3, 2, 1, 1, 2, 3, 3, 1, 2, 1, 1, 3, 2, 2, 3, 1, 3, 1, 3, 1, 3, 1, 2, 2, 3, 1, 1, 1, 2, 1, 2, 3, 2, 2, 3, 1, 1, 3, 1, 1, 3, 2, 1, 1, 3, 1, 3, 2, 3, 1, 3, 1, 3, 3, 2, 2, 3, 1, 2, 1, 3, 1, 1, 1, 3, 3, 3, 2, 3, 3, 2, 3, 3, 1, 2, 2, 3, 1, 1, 1, 3, 1, 3, 2, 2, 3, 1, 3, 2, 3, 1, 1, 2, 1, 1, 3, 2, 2, 2, 1, 1, 1, 3, 1, 1, 1, 2, 2, 3, 2, 1, 2, 3, 3, 3, 3, 2, 3, 1, 3, 2, 2, 1, 1, 2, 1, 3, 2, 1, 2, 2, 1, 1, 1, 2, 1, 3, 1, 2, 3, 2, 2, 2, 3, 2, 3, 1, 1, 1, 1, 3, 2, 3, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 1, 1, 2, 1, 1, 1, 1, 3, 3, 3, 2, 3, 1, 2, 3, 1, 1, 1, 3, 1, 2, 3, 1, 1, 1, 1, 3, 1, 3, 3, 2, 3, 2, 3, 3, 3, 2, 3, 1, 2, 3, 2, 3, 2, 1, 2, 3, 1, 3, 3, 1, 2, 3, 2, 3, 3, 1, 1, 2, 3, 3, 2, 1, 1, 3, 1, 3, 2, 3, 2, 3, 3, 2, 2, 3, 1, 3, 2, 3, 2, 3, 1, 1, 1, 3, 3, 3, 2, 1, 3, 3, 2, 1, 2, 1, 1, 3, 2, 2, 1, 2, 1, 2, 3, 3, 3, 1, 1, 2, 1, 3, 3, 3, 2, 2, 2, 3, 2, 2, 1, 1, 1, 2, 2, 2, 1, 3, 3, 3, 2, 1, 2, 3, 3, 3, 3, 2, 1, 1, 2, 3, 3, 3, 1, 2, 3, 3, 2, 3, 3, 2, 2, 1, 2, 2, 3, 2, 3, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 3, 2, 2, 2, 3, 2, 2, 1, 1, 3, 1, 1, 3, 3, 2, 2, 1, 2, 3, 2, 1, 1, 1, 1, 3, 3, 2, 3, 2, 1, 1, 1, 1, 1, 3, 3, 2, 3, 1, 1, 2, 3, 3, 2, 3, 3, 2, 1, 3, 3, 1, 3, 1, 3, 3, 3, 2, 1, 2, 3, 2, 3, 3, 3, 2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 3, 2, 2, 2, 2, 1]
# value = minimumMovement entryValues

# puts "resultado 2 > #{value}"

# entryValues = [1, 1, 3, 1, 2, 3, 2, 2, 1, 3, 3, 3, 1, 3, 2, 2, 3, 2, 2, 1, 2, 3, 2, 2, 1, 3, 3, 2, 2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, 3, 2, 3, 3, 1, 2, 2, 1, 1, 2, 3, 1, 1, 2, 3, 2, 3, 3, 2, 2, 3, 3, 3, 2, 2, 3, 2, 2, 1, 1, 3, 2, 3, 1, 2, 3, 3, 1, 2, 3, 3, 1, 3, 1, 1, 1, 3, 1, 3, 1, 3, 3, 3, 2, 2, 1, 3, 1, 2, 3, 2, 1, 1, 2, 1, 3, 2, 3, 3, 1, 2, 1, 3, 3, 1, 1, 2, 1, 3, 3, 2, 2, 2, 2, 2, 1, 3, 3, 2, 3, 1, 1, 1, 2, 3, 2, 1, 2, 1, 3, 2, 2, 2, 3, 3, 1, 1, 1, 3, 3, 1, 2, 3, 1, 3, 2, 2, 3, 1, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3, 3, 1, 2, 2, 2, 2, 3, 2, 1, 3, 2, 2, 3, 1, 1, 1, 2, 3, 3, 2, 2, 2, 2, 1, 1, 1, 2, 1, 1, 3, 1, 1, 3, 2, 3, 1, 3, 1, 2, 3, 2, 3, 1, 2, 1, 2, 3, 1, 1, 3, 3, 2, 3, 3, 2, 3, 2, 1, 3, 2, 3, 3, 2, 3, 1, 3, 1, 1, 3, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 3, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 2, 2, 3, 2, 3, 2, 3, 1, 3, 2, 1, 3, 2, 3, 2, 2, 1, 1, 2, 3, 2, 1, 3, 2, 1, 1, 3, 1, 1, 1, 3, 2, 3, 3, 2, 2, 3, 2, 3, 1, 3, 1, 3, 2, 3, 1, 3, 2, 1, 3, 3, 2, 2, 1, 2, 2, 1, 1, 3, 1, 2, 1, 2, 2, 3, 1, 2, 3, 1, 2, 3, 2, 2, 2, 2, 1, 2, 1, 3, 2, 2, 1, 3, 1, 2, 1, 1, 1, 2, 3, 2, 3, 3, 1, 1, 1, 2, 3, 3, 2, 1, 1, 3, 2, 3, 2, 1, 1, 1, 3, 2, 3, 3, 3, 3, 1, 2, 2, 2, 3, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 2, 1, 2, 3, 3, 1, 3, 1, 1, 2, 2, 2, 3, 2, 3, 1, 2, 3, 1, 2, 1, 1, 3, 3, 1, 1, 1, 2, 3, 3, 1, 1, 3, 2, 1, 2, 3, 3, 2, 3, 3, 3, 2, 1, 2, 3, 3, 1, 2]
# value = minimumMovement entryValues

# puts "resultado 3 > #{value}"

# entryValues = [1, 1, 3, 1, 2, 3, 2, 2, 1, 3, 3, 3, 1, 3, 2, 2, 3, 2, 2, 1, 2, 3, 2, 2, 1, 3, 3, 2, 2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, 3, 2, 3, 3, 1, 2, 2, 1, 1, 2, 3, 1, 1, 2, 3, 2, 3, 3, 2, 2, 3, 3, 3, 2, 2, 3, 2, 2, 1, 1, 3, 2, 3, 1, 2, 3, 3, 1, 2, 3, 3, 1, 3, 1, 1, 1, 3, 1, 3, 1, 3, 3, 3, 2, 2, 1, 3, 1, 2, 3, 2, 1, 1, 2, 1, 3, 2, 3, 3, 1, 2, 1, 3, 3, 1, 1, 2, 1, 3, 3, 2, 2, 2, 2, 2, 1, 3, 3, 2, 3, 1, 1, 1, 2, 3, 2, 1, 2, 1, 3, 2, 2, 2, 3, 3, 1, 1, 1, 3, 3, 1, 2, 3, 1, 3, 2, 2, 3, 1, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3, 3, 1, 2, 2, 2, 2, 3, 2, 1, 3, 2, 2, 3, 1, 1, 1, 2, 3, 3, 2, 2, 2, 2, 1, 1, 1, 2, 1, 1, 3, 1, 1, 3, 2, 3, 1, 3, 1, 2, 3, 2, 3, 1, 2, 1, 2, 3, 1, 1, 3, 3, 2, 3, 3, 2, 3, 2, 1, 3, 2, 3, 3, 2, 3, 1, 3, 1, 1, 3, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 3, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 2, 2, 3, 2, 3, 2, 3, 1, 3, 2, 1, 3, 2, 3, 2, 2, 1, 1, 2, 3, 2, 1, 3, 2, 1, 1, 3, 1, 1, 1, 3, 2, 3, 3, 2, 2, 3, 2, 3, 1, 3, 1, 3, 2, 3, 1, 3, 2, 1, 3, 3, 2, 2, 1, 2, 2, 1, 1, 3, 1, 2, 1, 2, 2, 3, 1, 2, 3, 1, 2, 3, 2, 2, 2, 2, 1, 2, 1, 3, 2, 2, 1, 3, 1, 2, 1, 1, 1, 2, 3, 2, 3, 3, 1, 1, 1, 2, 3, 3, 2, 1, 1, 3, 2, 3, 2, 1, 1, 1, 3, 2, 3, 3, 3, 3, 1, 2, 2, 2, 3, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 2, 1, 2, 3, 3, 1, 3, 1, 1, 2, 2, 2, 3, 2, 3, 1, 2, 3, 1, 2, 1, 1, 3, 3, 1, 1, 1, 2, 3, 3, 1, 1, 3, 2, 1, 2, 3, 3, 2, 3, 3, 3, 2, 1, 2, 3, 3, 1, 2]
# value = minimumMovement entryValues

# puts "resultado 4 > #{value}"

# entryValues = [1, 1, 3, 1, 2, 3, 2, 2, 1, 3, 3, 3, 1, 3, 2, 2, 3, 2, 2, 1, 2, 3, 2, 2, 1, 3, 3, 2, 2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 1, 3, 2, 3, 3, 1, 2, 2, 1, 1, 2, 3, 1, 1, 2, 3, 2, 3, 3, 2, 2, 3, 3, 3, 2, 2, 3, 2, 2, 1, 1, 3, 2, 3, 1, 2, 3, 3, 1, 2, 3, 3, 1, 3, 1, 1, 1, 3, 1, 3, 1, 3, 3, 3, 2, 2, 1, 3, 1, 2, 3, 2, 1, 1, 2, 1, 3, 2, 3, 3, 1, 2, 1, 3, 3, 1, 1, 2, 1, 3, 3, 2, 2, 2, 2, 2, 1, 3, 3, 2, 3, 1, 1, 1, 2, 3, 2, 1, 2, 1, 3, 2, 2, 2, 3, 3, 1, 1, 1, 3, 3, 1, 2, 3, 1, 3, 2, 2, 3, 1, 3, 1, 2, 2, 3, 3, 2, 3, 1, 3, 3, 1, 2, 2, 2, 2, 3, 2, 1, 3, 2, 2, 3, 1, 1, 1, 2, 3, 3, 2, 2, 2, 2, 1, 1, 1, 2, 1, 1, 3, 1, 1, 3, 2, 3, 1, 3, 1, 2, 3, 2, 3, 1, 2, 1, 2, 3, 1, 1, 3, 3, 2, 3, 3, 2, 3, 2, 1, 3, 2, 3, 3, 2, 3, 1, 3, 1, 1, 3, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 3, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 2, 2, 3, 2, 3, 2, 3, 1, 3, 2, 1, 3, 2, 3, 2, 2, 1, 1, 2, 3, 2, 1, 3, 2, 1, 1, 3, 1, 1, 1, 3, 2, 3, 3, 2, 2, 3, 2, 3, 1, 3, 1, 3, 2, 3, 1, 3, 2, 1, 3, 3, 2, 2, 1, 2, 2, 1, 1, 3, 1, 2, 1, 2, 2, 3, 1, 2, 3, 1, 2, 3, 2, 2, 2, 2, 1, 2, 1, 3, 2, 2, 1, 3, 1, 2, 1, 1, 1, 2, 3, 2, 3, 3, 1, 1, 1, 2, 3, 3, 2, 1, 1, 3, 2, 3, 2, 1, 1, 1, 3, 2, 3, 3, 3, 3, 1, 2, 2, 2, 3, 2, 2, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 2, 1, 2, 3, 3, 1, 3, 1, 1, 2, 2, 2, 3, 2, 3, 1, 2, 3, 1, 2, 1, 1, 3, 3, 1, 1, 1, 2, 3, 3, 1, 1, 3, 2, 1, 2, 3, 3, 2, 3, 3, 3, 2, 1, 2, 3, 3, 1, 2]
# value = minimumMovement entryValues

# puts "resultado 5 > #{value}"

puts "program finished"
