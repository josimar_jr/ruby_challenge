#!/bin/ruby

require 'json'
require 'stringio'

#
# Complete the 'countMeetings' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY firstDay
#  2. INTEGER_ARRAY lastDay
#

puts "\n\n\n\niniciando processamento\n\n\n"


def isInAgenda(agenda, dateValue)
    return agenda.has_key?(dateValue)
end


def getAgendasRestrictedToOneDay(agenda, firstDaysList, lastDaysList)
    position = 0
    diffLists = Hash.new

    while position < firstDaysList.size
        dateValueFirst = firstDaysList[position]
        dateValueLast = lastDaysList[position]

        # prioritizes investors with only one day available
        if (dateValueFirst == dateValueLast && !isInAgenda(agenda, dateValueFirst))
            agenda[dateValueFirst] = 1
        else
            # saves items difference to evaluate after
            difference = (dateValueLast - dateValueFirst)

            if difference > 0
                # keep a list of items with the difference
                # that will allow to have gone through the whole list only once
                if !(diffLists.has_key?(difference))
                    diffLists[difference] = []
                end

                diffLists[difference] << position
            end
        end
        position += 1
    end

    # puts "getAgendasRestrictedToOneDay >> #{agenda}"
    return diffLists
end


def evaluateMultipleDaysAgendaProgressively(agenda, firstDaysList, lastDaysList, listToEvaluate, numberOfInvestors)
    keysToEvaluate = listToEvaluate.keys.sort

    # goes through the diffence of days
    keysToEvaluate.each do |diffValue|

        # goes through the positions with the difference
        listToEvaluate[diffValue].each do |position|

            beginsAt = firstDaysList[position]
            endsAt = lastDaysList[position]
            evaluationDay = beginsAt
            investorWithAgenda = false

            # goes through the available days for the investor
            while (beginsAt <= evaluationDay && endsAt >= evaluationDay && !investorWithAgenda)

                # when doesnt exist adds to the agenda and exits the loop
                if !(agenda.has_key?(evaluationDay))
                    agenda[evaluationDay] = 1
                    investorWithAgenda = true
                end
                evaluationDay += 1
            end

            # breaks the loop anytime it reaches the number of investors
            if (agenda.size == numberOfInvestors)
                break
            end
        end

        # breaks the loop anytime it reaches the number of investors
        if (agenda.size == numberOfInvestors)
            break
        end
    end
end


def countMeetings(firstDaysList, lastDaysList)
    # Write your code here
    # puts "parameters 1 \n1 #{firstDaysList}"
    # puts "parameters 2 \n2 #{lastDaysList}"

    numberOfInvestors = firstDaysList.size
    arrangedMeetings = 0
    agenda = Hash.new

    listOfDifferences = getAgendasRestrictedToOneDay(agenda, firstDaysList, lastDaysList)
    puts "countMeetings >> agenda list #{agenda}"

    arrangedMeetings = agenda.size
    return arrangedMeetings if (arrangedMeetings == numberOfInvestors)

    puts "countMeetings >> continuando o processamento"
    evaluateMultipleDaysAgendaProgressively(agenda, firstDaysList, lastDaysList, listOfDifferences, numberOfInvestors)
    arrangedMeetings = agenda.size

    return arrangedMeetings
end

entry1 = [1, 1, 3]
entry2 = [1, 2, 3]

puts "\nprimeira chamada"

result = countMeetings entry1, entry2

puts "resultado > #{result}\n"


entry1 = [1, 1, 3]
entry2 = [1, 1, 3]

puts "\nsegunda chamada"

result = countMeetings entry1, entry2

puts "resultado > #{result}\n"
