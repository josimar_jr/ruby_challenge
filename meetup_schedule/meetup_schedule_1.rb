#!/bin/ruby

require 'json'
require 'stringio'

#
# Complete the 'countMeetings' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY firstDay
#  2. INTEGER_ARRAY lastDay
#

puts "\n\n\n\niniciando processamento\n\n\n"

def getAllPossibilities(firstDaysList, lastDaysList)

    agenda = Hash.new

    (firstDaysList.size).times do |position|

        beginsAt = firstDaysList[position]
        endsAt = lastDaysList[position]
        evaluationDay = beginsAt

        # goes through the available days for the investor
        while (beginsAt <= evaluationDay && endsAt >= evaluationDay)

            # when doesnt exist adds to the agenda and exits the loop
            if !(agenda.has_key?(evaluationDay))
                agenda[evaluationDay] = []
            end

            agenda[evaluationDay] << position
            evaluationDay += 1
        end
    end

    return agenda
end

def countMeetings(firstDaysList, lastDaysList)
    agenda = getAllPossibilities()

    return firstDaysList.size if (agenda.size > firstDaysList.size)

    return agenda.size
end

entry1 = [1, 1, 3]
entry2 = [1, 2, 3]

puts "\nprimeira chamada"

result = countMeetings entry1, entry2

puts "resultado > #{result}\n"


entry1 = [1, 1, 3]
entry2 = [1, 1, 3]

puts "\nsegunda chamada"

result = countMeetings entry1, entry2

puts "resultado > #{result}\n"
