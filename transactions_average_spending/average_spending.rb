#!/bin/ruby

require 'json'
require 'stringio'


#
# Complete the 'getUserTransaction' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER uid
#  2. STRING txnType
#  3. STRING monthYear
#
#  https://jsonmock.hackerrank.com/api/transactions/search?txnType=
#

require 'date'
require 'net/http'
require 'uri'

def getUriContent(baseUri)
    uri = URI.parse(baseUri)
    response = Net::HTTP.get_response(uri)
    json_data = JSON.parse(response.body)

    return json_data["data"], json_data["total"], json_data["page"].to_i, json_data["total_pages"]
end


def doRequestWithUser(uid, txnType, page=1)
    baseUri = "https://jsonmock.hackerrank.com/api/transactions/search?txnType=#{txnType}&userId=#{uid}&page=#{page}"
    return getUriContent(baseUri)
end


def toIntMonthAndYear(str_month)
    month = str_month[0, 2].to_i
    year = str_month[3, 4].to_i
    return month, year
end


def isMonthYearOk(timestampValue, monthToFilter, yearToFilter)
    # dateFromStamp = Time.at(timestampValue).to_datetime
    dateFromStamp = DateTime.strptime(timestampValue.to_s, '%Q')
    # p "data: #{dateFromStamp.month} #{dateFromStamp.year} full #{dateFromStamp}"
    return (dateFromStamp.month == monthToFilter && dateFromStamp.year == yearToFilter)
end


def getAllDataForUser(uid, txnType, monthYear)
    page = 1
    pagesLimit = 1
    allUserData = []
    total = 0

    month, year = toIntMonthAndYear(monthYear)
    #p "month = #{month} year = #{year}"

    while page <= pagesLimit
        data, total, page, pagesLimit = doRequestWithUser(uid, txnType, page)

        data.each do |item|
            if (isMonthYearOk(item["timestamp"], month, year))
                # p "item do mes #{item["id"]}"
                allUserData << item
            end
        end

        page += 1
    end


    return allUserData, allUserData.size
end


def amountToNumber(strValue)
    new_str = strValue.gsub("$", "")
    new_str = new_str.gsub(",", "")
    value = new_str.to_f
    # p "from #{strValue} to #{new_str} to #{value}"
    return value
end


def getAverageSpendingValueFor(uid, monthYear, spendingType="debit")
    totalSpendingValue = 0

    # request debits for monthYear and txnType=debit
    data, totalItems = getAllDataForUser(uid, spendingType, monthYear)
    # puts "getAverageSpendingValue >> qt item #{data.size} total #{totalItems}"

    # calculates AVERAGE spending at the month
    data.each {|item| totalSpendingValue += amountToNumber(item["amount"]) }

    average = 0
    if totalItems > 0
        average = (totalSpendingValue / totalItems)
    end

    return average, data
end

def getItemsAboveThreshold(threshold, listOfItem, property)
    itemsAbove = []

    listOfItem.each do |item|
        # p "item = #{item}"
        amount = item["amount"]
        value = amountToNumber(amount)
        if (value > threshold)
            itemsAbove << item["id"]
        end
    end
    return itemsAbove
end

def getUserTransaction(uid, txnType, monthYear)
    # Write your code here
    # puts "getUserTransaction > user #{uid} type #{txnType} month #{monthYear}"
    debitType = 'debit'

    # get average spending info
    averageSpending, debitData = getAverageSpendingValueFor(uid, monthYear, debitType)
    p "getUserTransaction > average #{averageSpending}"

    txnData = 0
    if txnType == debitType
        txnData, totalItems = debitData, debitData.size
    else
        # request data for the parameters values uid, txnType and monthYear
        txnData, totalItems = getAllDataForUser(uid, txnType, monthYear)
    end
    # p "getUserTransaction > userdata #{txnData.size}"

    itemsAboveAverage = getItemsAboveThreshold(averageSpending, txnData, "amount")

    # evaluates the items greater than or equal to the AVERAGE
    return [-1] if (itemsAboveAverage.size == 0)

    # order items with above amount as ascendent ids
    return itemsAboveAverage.sort
end


puts "testando a solucao\n\n\n"

result = getUserTransaction 4, "debit", "02-2019"

puts "resultado #{result}\n"

