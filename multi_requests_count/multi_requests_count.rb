# read the string filename
filename = gets.strip
outputFile = File.open("req_#{filename}", 'w')

def getTimeStamp(line)
    timeStamp = line[/[0-9]{0,2}\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Ago|Sep|Oct|Nov|Dec)\/[0-9]{4}:[0-9]{2}:[0-9]{2}:[0-9]{2}/]
    return timeStamp
end

allTimeStamps = Hash.new

IO.foreach(filename) do |line|
    timeStamp = getTimeStamp(line)

    if allTimeStamps.has_key?(timeStamp)
        if allTimeStamps[timeStamp] == 0
            allTimeStamps[timeStamp] = 1

            #prints the timestamp
            outputFile.write "#{timeStamp}\n"
        end
    else
        allTimeStamps[timeStamp] = 0
    end
end

outputFile.close()



# fptr = File.open(ENV['OUTPUT_PATH'], 'w')

# firstDay_count = gets.strip.to_i

# firstDay = Array.new(firstDay_count)

# firstDay_count.times do |i|
#     firstDay_item = gets.strip.to_i
#     firstDay[i] = firstDay_item
# end

# lastDay_count = gets.strip.to_i

# lastDay = Array.new(lastDay_count)

# lastDay_count.times do |i|
#     lastDay_item = gets.strip.to_i
#     lastDay[i] = lastDay_item
# end

# result = countMeetings firstDay, lastDay

# fptr.write result
# fptr.write "\n"

# fptr.close()